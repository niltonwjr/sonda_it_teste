﻿using NHibernate;
using SondaIT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SondaIT.Controllers
{
    public class VendaController : Controller
    {

        // GET: Venda
        public ActionResult Lista()
        {
            using (ISession session = DbNHibernateHelper.OpenSession())
            {
                var vendas = session.Query<Venda>().ToList();
                return View(vendas);
            }
        }

        // GET: Venda/Create
        public ActionResult Create()
        {
            ViewBag.DataHoraAtual = DateTime.Now.ToString("dd/MM/yyyy HH:mm ");
            return View();
        }

        // POST: Venda/Create
        [HttpPost]
        public ActionResult Create(Venda venda)
        {
            try
            {
                using (ISession session = DbNHibernateHelper.OpenSession())
                {
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Save(venda);
                        transaction.Commit();
                    }
                }
                return RedirectToAction($"AdicionarProdutos/{venda.Id}","VendaProdutos");
            }
            catch 
            {
                return View();
            }
        }

    }
}
