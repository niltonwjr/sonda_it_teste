﻿using NHibernate;
using SondaIT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SondaIT.Controllers
{
    public class VendaProdutosController : Controller
    {
        // GET: VendaProdutos
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AdicionarProdutos(string Id)
        {
            if (!string.IsNullOrEmpty(Id))
            {
                ViewBag.IdVenda = Id;
                using (ISession session = DbNHibernateHelper.OpenSession())
                {
                    var prodDisp = session.Query<Produto>()
                        .Where(p => p.Ativo == (int)SituacaoProduto.Ativo)
                        .ToList();

                     var prodVenda = session.Query<VendaProdutos>()
                        .Where(p => p.IdVenda == int.Parse(Id))
                        .ToList();

                    ViewBag.ProdutosDisponiveis = prodDisp;
                    ViewBag.ProdutosDaVenda = prodVenda;
                    decimal valorTotal = 0;
                    if (prodVenda.Count > 0)
                    {
                        foreach (var p in prodVenda)
                        {
                            foreach (var pd in prodDisp)
                                if (p.IdProduto == pd.Id)
                                    valorTotal += p.QuantidadeProduto * pd.Valor;
                        }
                    }
                    ViewBag.ValorTotal = valorTotal;
                }
                return View();
            }
            else
            {
                return RedirectToAction($"Lista", "Venda");
            }
        }

        // POST: Produto/Create
        [HttpPost]
        public ActionResult AdicionarProdutos(VendaProdutos produtoVenda)
        {
            try
            {
                using (ISession session = DbNHibernateHelper.OpenSession())
                {  
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Save(produtoVenda);
                        transaction.Commit();
                    }
                }
                return RedirectToAction($"AdicionarProdutos/" + produtoVenda.IdVenda);
            }
            catch
            {
                return View();
            }
        }
    }
}