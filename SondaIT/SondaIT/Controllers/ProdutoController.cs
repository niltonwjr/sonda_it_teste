﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Linq;
using SondaIT.Models;

namespace SondaIT.Controllers
{
    public class ProdutoController : Controller
    {
        // GET: Produto
        public ActionResult Lista()
        {
            using (ISession session = DbNHibernateHelper.OpenSession())
            {
                var alunos = session.Query<Produto>().ToList();
                return View(alunos);
            }
        }

        // GET: Produto/Create/5
        public ActionResult Create()
        {
            return View();
        }

        // POST: Produto/Create
        [HttpPost]
        public ActionResult Create(Produto produto)
        {
            try
            {
                produto.Ativo = (int)SituacaoProduto.Ativo;
                using (ISession session = DbNHibernateHelper.OpenSession())
                {
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Save(produto);
                        transaction.Commit();
                    }
                }
                return RedirectToAction("Lista");
            }
            catch
            {
                return View();
            }
        }

        // GET: Produto/Edit/5
        public ActionResult Edit(int id)
        {
            using (ISession session = DbNHibernateHelper.OpenSession())
            {
                var produto = session.Get<Produto>(id);
                return View(produto);
            }
        }

        // POST: Produto/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Produto produto)
        {
            try
            {
                using (ISession session = DbNHibernateHelper.OpenSession())
                {
                    var produtoEditado = session.Get<Produto>(id);
                    produtoEditado.Ativo = (int)SituacaoProduto.Ativo;
                    produtoEditado.Codigo = produto.Codigo;
                    produtoEditado.Descricao = produto.Descricao;
                    produtoEditado.Valor = produto.Valor;
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Save(produtoEditado);
                        transaction.Commit();
                    }
                }
                return RedirectToAction("Lista");
            }
            catch
            {
                return View();
            }
        }

        // GET: Produto/Delete/5
        public ActionResult Inactive(int id)
        {
            using (ISession session = DbNHibernateHelper.OpenSession())
            {
                var produto = session.Get<Produto>(id);
                return View(produto);
            }
        }

        // POST: Produto/Delete/5
        [HttpPost]
        public ActionResult Inactive(int id, Produto produto)
        {
            try
            {
                using (ISession session = DbNHibernateHelper.OpenSession())
                {
                    var produtoEditado = session.Get<Produto>(id);
                    produtoEditado.Ativo = (int)SituacaoProduto.Inativo;                   
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Save(produtoEditado);
                        transaction.Commit();
                    }
                }
                return RedirectToAction("Lista");
            }
            catch
            {
                return View();
            }
        }
    }
}
