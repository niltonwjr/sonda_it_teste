﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SondaIT.Models
{
    public enum SituacaoProduto
    {
        Inativo = 0,
        Ativo = 1
    }
    public class Produto
    {
        public virtual int Id { get; set; }
        public virtual string Codigo { get; set; }
        public virtual string Descricao { get; set; }
        public virtual decimal Valor { get; set; }
        public virtual int Ativo { get; set; }

        public virtual string GetDescricaoAtivo()
        {
            return this.Ativo == (int)SituacaoProduto.Ativo ? "Sim" : "Não";
        }

    }
}