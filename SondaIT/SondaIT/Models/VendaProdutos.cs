﻿namespace SondaIT.Models
{
    public class VendaProdutos
    {
        public virtual int Id { get; set; }
        public virtual int IdVenda { get; set; }
        public virtual int IdProduto { get; set; }
        public virtual int QuantidadeProduto { get; set; }
    }
}
