﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SondaIT.Models.POCO
{
    public class ProdutoMap : ClassMap<Produto>
    {
        public ProdutoMap()
        {
            Id(x => x.Id);
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Descricao).Not.Nullable();
            Map(x => x.Valor).Not.Nullable();
            Map(x => x.Ativo).Not.Nullable();
            Table("PRODUTO");
        }
    }
}