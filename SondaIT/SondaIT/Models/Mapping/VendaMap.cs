﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SondaIT.Models.Mapping
{
    public class VendaMap : ClassMap<Venda>
    {
        public VendaMap()
        {
            Id(x => x.Id);
            Map(x => x.Data).Not.Nullable();
            Table("VENDA");
        }
    }
}