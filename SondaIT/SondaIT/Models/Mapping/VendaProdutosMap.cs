﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SondaIT.Models.Mapping
{

    public class VendaProdutosMap : ClassMap<VendaProdutos>
    {
        public VendaProdutosMap()
        {
            Id(x => x.Id);
            Map(x => x.IdVenda).Not.Nullable();
            Map(x => x.IdProduto).Not.Nullable();
            Map(x => x.QuantidadeProduto);
            Table("VENDAPRODUTOS");
        }
    }

}