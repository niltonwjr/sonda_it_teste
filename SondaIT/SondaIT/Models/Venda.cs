﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SondaIT.Models
{
    public class Venda
    {
        public virtual int Id { get; set; }
        public virtual DateTime Data { get; set; }        
        public virtual List<VendaProdutos> ProdutosDaVenda { get; set; }

        public Venda()
        {
            this.ProdutosDaVenda = new List<VendaProdutos>();
        }
    }
}